/*
 *  list.h
 *  
 *
 *  Jean Goulet 
 *
 */

#pragma once
#include <iostream>
#include <cassert>

///////////////////////////////////////////////////////////
/*Description de la representation

 Chaine double de cellules 
 Une cellule supplementaire a la fin,
 nommee APRES en memoire automatique
 DEBUT pointe sur la premiere cellule
 SIZE contient le nombre d'elements dans la liste
 
 
 iterator: pointeur sur la cellule concernee,
 la fin est un pointeur sur la cellule de queue
 
 reverse_iterator: pointeur sur la cellule qui 
 suit la cellule concernee 
 (rbegin: la queue, rend: la premiere)
 
 RESTRICTION par rapport a la SL
 pas de const_iterator, donc pas d'iteration possible
 sur une list const.

*//////////////////////////////////////////////////////////


template <typename TYPE>
class list{
		
private:
	struct cellule{
		TYPE CONTENU;
		cellule *SUIV,*PREC;
        cellule(const TYPE& C,cellule*S=nullptr,cellule*P=nullptr)
            :CONTENU(C),SUIV(S),PREC(P){}
        ~cellule(){PREC=SUIV=nullptr;}
	};
    cellule *DEBUT;
    cellule APRES;
	size_t SIZE;
    
    //fonctions generatrices privees
    
    //inserer physiquement avant cette cellule - retourne nouvelle cellule
	cellule* insert(cellule*,const TYPE&);
    //enlever cette cellule - retourne cellule suivante
    cellule* erase(cellule*);
	
public:
    class iterator;
    class const_iterator;           //à venir!
    class reverse_iterator;
    class const_reverse_iterator;   //à venir!
	
	list();
	~list();
	list(const list&);
    list(std::initializer_list<TYPE>);
	list& operator=(const list&);
    void swap(list&);
	
	iterator insert(iterator,const TYPE&);
	iterator erase(iterator);
	reverse_iterator insert(reverse_iterator,const TYPE&);
	reverse_iterator erase(reverse_iterator);
	
	void push_back(const TYPE&);
	void pop_back();
	void push_front(const TYPE&);
	void pop_front();
	const TYPE& back()const;
	const TYPE& front()const;
	TYPE& back();
	TYPE& front();
	
	void clear();
	size_t size()const;
	bool empty()const;
	
	iterator begin();
	iterator end();
	reverse_iterator rbegin();
	reverse_iterator rend();
    
    //algorithmes
    void reverse();
    void splice(iterator,list&);
    void resize(size_t,const TYPE& = TYPE());
	
    //fonctions de mise au point (code jetable)
	void afficher()const;
};

//
//les classes d'iteration
//iterator est donné
//reverse_iterator à coder
//

template <typename TYPE>
class list<TYPE>::iterator{
    friend class list<TYPE>;
private:
    cellule* POINTEUR;
public:
    iterator(cellule*c=nullptr):POINTEUR(c){}
    TYPE& operator*()const{return POINTEUR->CONTENU;}
    TYPE* operator->()const{return &(POINTEUR->CONTENU);}
    iterator& operator++(){POINTEUR=POINTEUR->SUIV;return *this;} //++i
    iterator operator++(int){   //i++
        iterator ret(*this);
        POINTEUR=POINTEUR->SUIV;
        return ret;
        }
    iterator& operator--(){POINTEUR=POINTEUR->PREC;return *this;} //--i
    iterator operator--(int){   //i--
        iterator ret(*this);
        POINTEUR=POINTEUR->PREC;
        return ret;
        }
    bool operator==(const iterator&droite)const{
        return POINTEUR==droite.POINTEUR;}
    bool operator!=(const iterator&droite)const{
        return POINTEUR!=droite.POINTEUR;}
};


#include "list2.h"


///
template <typename TYPE>
list<TYPE>::list():DEBUT(&APRES),APRES(TYPE()),SIZE(0){}
	
template <typename TYPE>
list<TYPE>::~list(){clear();}

template <typename TYPE>
list<TYPE>::list(const list& droite):list(){
    *this=droite;
}

template <typename TYPE>
list<TYPE>::list(std::initializer_list<TYPE> droite):list(){
    for(const auto& x:droite)push_back(x);
}

template <typename TYPE>
typename list<TYPE>::iterator list<TYPE>::insert(iterator i,const TYPE& x){
	return iterator(insert(i.POINTEUR,x));
}

template <typename TYPE>
typename list<TYPE>::iterator list<TYPE>::erase(iterator i){
	return iterator(erase(i.POINTEUR));
}

template <typename TYPE>
typename list<TYPE>::reverse_iterator list<TYPE>::insert(reverse_iterator i,const TYPE& x){
	insert(i.POINTEUR,x);
    return i;
}

template <typename TYPE>
typename list<TYPE>::reverse_iterator list<TYPE>::erase(reverse_iterator i){
	erase(i.POINTEUR->PREC);
    return i;
}

template <typename TYPE>
void list<TYPE>::push_back(const TYPE& x){
	insert(end(),x);
	}

template <typename TYPE>
void list<TYPE>::pop_back(){
	erase(rbegin());
	}

template <typename TYPE>
void list<TYPE>::push_front(const TYPE& x){
	insert(begin(),x);
	}

template <typename TYPE>
void list<TYPE>::pop_front(){
	erase(begin());
}

template <typename TYPE>
const TYPE& list<TYPE>::back()const{
	return *rbegin();
	}

template <typename TYPE>
const TYPE& list<TYPE>::front()const{
	return *begin();
	}

template <typename TYPE>
TYPE& list<TYPE>::back(){
	return *rbegin();
	}

template <typename TYPE>
TYPE& list<TYPE>::front(){
	return *begin();
	}


template <typename TYPE>
void list<TYPE>::clear(){
    while(DEBUT!=&APRES){
        DEBUT=erase(DEBUT);
        }
	}

template <typename TYPE>
size_t list<TYPE>::size()const{
	return SIZE;
}

template <typename TYPE>
bool list<TYPE>::empty()const{
	return SIZE==0;
}


//
// gestion de l'iteration
//

template <typename TYPE>
typename list<TYPE>::iterator list<TYPE>::begin(){
	return iterator(DEBUT);
	}

template <typename TYPE>
typename list<TYPE>::iterator list<TYPE>::end(){
    return iterator(&APRES);
	}


//
// algorithmes
//


template <typename TYPE>
void list<TYPE>::resize(size_t N,const TYPE& VAL){
    iterator it=begin();
    for(size_t i=0;i<N;++i,++it)
        if(it==end())it=insert(it,VAL);
    while(it!=end())it=erase(it);
}


///////////////////////////////////////////////////////////
//code jetable

template <typename TYPE>
void list<TYPE>::afficher()const{
    using namespace std;

    string s="s) ";
    if(SIZE<2)s=") -";
	cellule* p=DEBUT;
	size_t i=0,skipde=SIZE,skipa=0;
    if(skipde>5)skipde=5;
    if(skipa<SIZE-5)skipa=SIZE-5;
    cout<<"-----list "<<this<<" ("<<SIZE<<" element"<<s<<"-----"<<endl;
    cout<<"DEBUT: "<<DEBUT;
    cout<<"  APRES: "<<&APRES<<endl;
	while(p!=&APRES){
        if(i<skipde || i>skipa)
            cout<<i<<"=> <"<<p->CONTENU
            <<","<<p->SUIV<<","<<p->PREC
            <<">"<<endl;
        else if(i==skipde)
            cout<<"   ....."<<endl;
		i++;
		p=p->SUIV;
		}
    cout<<"APRES: <???,"<<APRES.SUIV<<","<<APRES.PREC<<">"<<endl;
	cout<<"-------------------------------------------"<<endl;
	}

///////////////////////////////////////////////////////////
//algorithme de tri avec des iterateurs generaux

template <typename iter>
void trier(iter DEB,iter FIN){
    size_t nb=0;
    iter i=DEB,j=FIN,PIVOT=DEB;
    //choisir l'element du milieu comme pivot
    for(;i!=j;++nb,++i){
        ++nb;
        if(i==--j)break;
    }
    if(nb<2)return;  //rien a trier
    std::swap(*DEB,*i);
    //separer la liste en deux  {<PIVOT,>=PIVOT}
    ++(i=DEB);
    j=FIN;
    while(i!=j)
        if(*i<*PIVOT)++i;
        else if(*--j<*PIVOT)std::swap(*i++,*j);
    --i;
    std::swap(*PIVOT,*i);
    //trier les deux sous-listes
    trier(DEB,i);
    trier(j,FIN);
}


//

