//
//  list2.h
//
//  Jean Goulet
//  Copyleft  2022 UdeS
//

#ifndef list2_h
#define list2_h

//
//fonctions generatrices privees
//

template <typename TYPE>
typename list<TYPE>::cellule* list<TYPE>::insert(cellule* c,const TYPE& VAL){
    
    cellule* nCellule = new cellule(VAL, c, c->PREC);

    if (DEBUT == c) {
        DEBUT = nCellule;
    }
    else {
        c->PREC->SUIV = nCellule;
    }
    c->PREC = nCellule;
    ++SIZE;
   
    return nCellule;
}

template <typename TYPE>
typename list<TYPE>::cellule* list<TYPE>::erase(cellule* c){

    cellule* av = c->PREC;
    cellule* ap = c->SUIV;

    if (APRES.PREC == nullptr) {
        return &APRES;
    }
  
    if (DEBUT == c) {
        DEBUT = ap;
    }
    else {
        c->PREC->SUIV = ap;
    }

    c->SUIV->PREC = av;
    delete c;
    --SIZE;

    if (empty()) {
        
        DEBUT = &APRES;
        APRES.PREC = nullptr;
        SIZE = 0;
    }

    return ap;
}

//
//affectateur de list
//

template <typename TYPE>
list<TYPE>& list<TYPE>::operator=(const list<TYPE>& droite){
    if (&droite == this)return *this;

    cellule* c = droite.DEBUT;
    
    clear();
    for(int i = 0; i < droite.SIZE; ++i) {
        
        push_back(c->CONTENU);
        c = c->SUIV;
    }

    return *this;
}

//
// reverse_iterator
//

template <typename TYPE>
class list<TYPE>::reverse_iterator{
    friend class list<TYPE>;
private:
    cellule* POINTEUR;
public:
    reverse_iterator(cellule*c=nullptr):POINTEUR(c){}
    TYPE& operator*()const { return POINTEUR->PREC->CONTENU;}
    TYPE* operator->()const{return &(POINTEUR->PREC->CONTENU);}
    reverse_iterator& operator++() { POINTEUR = POINTEUR->PREC; return *this; } //++i
    reverse_iterator operator++(int) {   //i++
        reverse_iterator ret(*this);
        POINTEUR = POINTEUR->PREC;
        return ret;
    }
    reverse_iterator& operator--(){POINTEUR=POINTEUR->SUIV;return *this;} //--i
    reverse_iterator operator--(int) {   //i--
        reverse_iterator ret(*this);
        POINTEUR = POINTEUR->SUIV;
        return ret;
    }
    bool operator==(const reverse_iterator&droite)const{
        return POINTEUR==droite.POINTEUR;}
    bool operator!=(const reverse_iterator&droite)const{
        return POINTEUR!=droite.POINTEUR;}
};

//
// gestion de l'itération inversée (rbegin et rend)
//

template <typename TYPE>
typename list<TYPE>::reverse_iterator list<TYPE>::rbegin(){
    return reverse_iterator(&APRES);
}

template <typename TYPE>
typename list<TYPE>::reverse_iterator list<TYPE>::rend(){
    return reverse_iterator(DEBUT);
}

//
//algorithm splice
//

template <typename TYPE>
void list<TYPE>::splice(iterator i, list& L) {

    if (!L.empty()) {
        
        if (i.POINTEUR->PREC != nullptr) {

            i.POINTEUR->PREC->SUIV = L.DEBUT;
            L.DEBUT->PREC = i.POINTEUR->PREC;
        }
        else {
            DEBUT = L.DEBUT;
        }
          
        L.APRES.PREC->SUIV = i.POINTEUR;
        i.POINTEUR->PREC = L.APRES.PREC;
        SIZE += L.SIZE;
        
        L.SIZE = 0;
        L.DEBUT = &L.APRES;
        L.APRES.PREC = nullptr;
    }
       
}

//
//algorithme reverse
//

template <typename TYPE>
void list<TYPE>::reverse(){
    auto nFin = DEBUT;
    DEBUT = APRES.PREC;
    APRES.PREC = nFin;
    nFin->PREC = &APRES;
    DEBUT->SUIV = nullptr;

    for(auto i = begin() ; i != end(); ++i) {
        cellule* pt = i.POINTEUR->SUIV;
        i.POINTEUR->SUIV = i.POINTEUR->PREC;
        i.POINTEUR->PREC = pt;
    }

}

#endif /* list2_h */
